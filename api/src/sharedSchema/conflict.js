module.exports = {
  $id: "conflict",
  description: "Conflict",
  type: "object",
  required: ["statusCode", "error", "message"],
  properties: {
    statusCode: { type: "integer", example: 409 },
    error: { type: "string", example: "Conflict" },
    message: { type: "string" }
  }
}
