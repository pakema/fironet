const fp = require("fastify-plugin")

module.exports = fp(async function (fastify) {
  fastify.setErrorHandler(function (error, request, reply) {
    const { log } = request
    log.error("error:", error.toString())

    if (error && error.isBoom) {
      reply
        .code(error.output.statusCode)
        .type("application/json")
        .headers(error.output.headers)
        .send(error.output.payload)

      return
    }

    if (error) {
      error.message = error.stack || error.message
      reply.send(error)
    } else {
      reply.send(new Error("Got non-error: " + error))
    }
  })
})
