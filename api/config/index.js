module.exports = {
  currencies: {
    usd: "usd",
    eur: "eur",
    uah: "uah"
  },
  rates: {
    usd: 1,
    eur: 1.17,
    uah: 0.035
  }
}
