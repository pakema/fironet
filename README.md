#Fironet - Currency exchange

## Installation

### Backend

Root directory: `api`
API documentation: `/swagger`
1. `npm i`
2. `npm run start`

### Frontend

Root directory: `vue`
1. `npm i`
2. `npm run serve`