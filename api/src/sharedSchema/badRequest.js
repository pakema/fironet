module.exports = {
  $id: "badRequest",
  description: "Bad Request",
  type: "object",
  required: ["statusCode", "error", "message"],
  properties: {
    statusCode: { type: "integer", example: 400 },
    error: { type: "string", example: "Bad Request" },
    message: { type: "string" }
  }
}
