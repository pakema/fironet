const config = require("../../config")
const { schema } = require("../schema/Convert")
const Decimal = require("decimal.js-light")

/**
 * @param {number} amount
 * @param {string} from
 * @param {string} to
 * @return {number}
 */
function convert(amount, from, to) {
  const decimal = new Decimal(amount)
  const rateFrom = config.rates[from]
  const rateTo = config.rates[to]
  const result = decimal.times(rateFrom).dividedBy(rateTo).toFixed(2)

  return result
}

async function handler(request) {
  const { query } = request
  const amount = parseFloat(query.amount)
  const from = query.from
  const to = query.to

  return {
    amount: convert(amount, from, to)
  }
}

module.exports = async function (fastify) {
  fastify.get("/", { schema }, handler)
}
