module.exports = {
  $id: "forbidden",
  description: "Forbidden",
  type: "object",
  required: ["statusCode", "error", "message"],
  properties: {
    statusCode: { type: "integer", example: 403 },
    error: { type: "string", example: "Forbidden" },
    message: { type: "string" }
  }
}
