module.exports = {
  env: {
    "commonjs": true,
    "es6": true,
    "node": true
  },
  extends: [
    "eslint:recommended",
    "plugin:node/recommended",
    "plugin:import/errors",
    "plugin:import/warnings"
  ],
  globals: {
    "Atomics": "readonly",
    "SharedArrayBuffer": "readonly"
  },
  parserOptions: {
    "ecmaVersion": 2018,
    "sourceType": "module"
  },
  plugins: [
    "node",
    "import"
  ],
  rules: {
    "indent": ["warn", 2, { "SwitchCase": 1 }],
    "semi": ["error", "never"],
    "quotes": ["error", "double"],
    "comma-dangle": ["error", "never"],
    "object-curly-spacing": ["error", "always"],
    "no-unused-vars": "warn",
    "node/exports-style": ["error", "module.exports"],
    "node/file-extension-in-import": ["error", "always"]
  }
}
