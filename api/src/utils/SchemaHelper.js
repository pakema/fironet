function make404(resource) {
  return {
    description: "Not Found",
    type: "object",
    required: ["statusCode", "error", "message"],
    properties: {
      statusCode: { type: "integer", example: 404 },
      error: { type: "string", example: "Not Found" },
      message: { type: "string", example: `${resource}.not_found` }
    }
  }
}

function make409(resource, fields) {
  return {
    description: "Conflict",
    type: "object",
    required: ["statusCode", "error", "message"],
    properties: {
      statusCode: { type: "integer", example: 409 },
      error: { type: "string", example: "Conflict" },
      message: { type: "string", example: `${resource}.conflicts.{${fields}}` }
    }
  }
}

module.exports = {
  make404,
  make409
}
