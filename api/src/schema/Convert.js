const config = require("../../config")
const SchemaHelper = require("../utils/SchemaHelper")

const model = {
  type: "object",
  required: ["amount"],
  properties: {
    amount: { type: "number" }
  }
}

const schema = {
  description: "Currency exchange",
  summary: "Currency exchange",
  tags: ["convert"],
  query: {
    type: "object",
    required: ["amount", "from", "to"],
    properties: {
      amount: { type: "number" },
      from: { type: "string", enum: Object.keys(config.currencies) },
      to: { type: "string", enum: Object.keys(config.currencies) }
    }
  },
  response: {
    200: model,
    404: SchemaHelper.make404("currency")
  }
}


module.exports = {
  model,
  schema
}
