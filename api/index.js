const Fastify = require("fastify")

// read env file
require("dotenv").config()

// check env
function checkEnvs(envs) {
  for (const env of envs) {
    console.log(env, process.env[env])
    if (!process.env[env]) {
      throw Error(`${env} must be set`)
    }
  }
}

checkEnvs([
  "NODE_ENV",
  "PORT",
  "ADDRESS",
  "BASE_URL"
])

const fastify = Fastify({
  logger: {
    timestamp: () => `,"time":${new Date().toLocaleString()}`,
    level: "info",
    base: null
  },
  ignoreTrailingSlash: false,
  bodyLimit: 1024 * 1024 * 5 // 5 mb
})

if (process.env.NODE_ENV === "development") {
  fastify.register(require("./src/plugins/cors"))
}

// sharedSchema
fastify.addSchema(require("./src/sharedSchema/badRequest"))
fastify.addSchema(require("./src/sharedSchema/conflict"))

fastify.addSchema(require("./src/sharedSchema/forbidden"))
fastify.addSchema(require("./src/sharedSchema/notFound"))

fastify.register(require("./src/plugins/boom"))
fastify.register(require("./src/plugins/swagger"))

// api
fastify.register(require("./src/routes"), { prefix: "api" })

// run server
fastify.listen(process.env.PORT, process.env.ADDRESS, (err) => {
  if (err) {
    fastify.log.error(err)
    throw err
  }
})
