module.exports = {
  $id: "notFound",
  description: "Not Found",
  type: "object",
  required: ["statusCode", "error", "message"],
  properties: {
    statusCode: { type: "integer", example: 404 },
    error: { type: "string", example: "Not Found" },
    message: { type: "string" }
  }
}
