const fp = require("fastify-plugin")
const swagger = require("fastify-swagger")

module.exports = fp(async function (fastify) {
  fastify.register(swagger, {
    routePrefix: "/swagger",
    swagger: {
      info: {
        title: "API Swagger",
        description: "Swagger currency exchange API",
        version: "1.0.0"
      },
      externalDocs: {
        url: "http://127.0.0.1:3001/swagger",
        description: "Swagger"
      },
      host: "127.0.0.1:3001",
      schemes: ["http"],
      consumes: ["application/json"],
      produces: ["application/json"],
      tags: [],
      definitions: {
        Convert: require("../schema/Convert").model
      }
    },
    exposeRoute: true
  })
})
